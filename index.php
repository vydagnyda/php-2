<?php

$amount = 100; //įvesta suma nusiimti pinigų iš bankomato

$balance = 500; //banko kortelės pinigų suma

$loanSelected = true; //pasirinkta imti paskolą

if ($amount <= $balance) {
    echo 'Pasiimkite pinigus';
    $balance = $balance - $amount;
} else {
    echo 'Nepakanka lėšų operacijai atlikti!';
}